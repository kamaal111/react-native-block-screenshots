import {NativeModules, Platform} from 'react-native'

const {RNBlockScreenshots} = NativeModules

export default {
  disableScreenshot: () => {
    if (Platform.OS !== 'android') {
      return console.log('is iOS')
    }
    return RNBlockScreenshots.disableScreenshot()
  },
  enableScreenshot: () => {
    if (Platform.OS !== 'android') {
      return console.log('is iOS')
    }
    return RNBlockScreenshots.enableScreenshot()
  }
}
