
// RNBlockScreenshotsModule.java

package com.rnblockscreenshots;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;

public class RNBlockScreenshotsModule extends ReactContextBaseJavaModule {

  @Override
  public String getName() {
    return "RNBlockScreenshots";
  }

  private static ReactApplicationContext reactContext;

  public RNBlockScreenshotsModule(ReactApplicationContext context) {
    super(context);
    reactContext = context;
  }

  @ReactMethod
  public void disableScreenshot() {
    final Activity activity = getCurrentActivity();
    if (activity == null) {
      return;
    }
    activity.runOnUiThread(new Runnable() {
      @Override
      public void run() {
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
      }
    });
  }

  @ReactMethod
  public void enableScreenshot() {
    final Activity activity = getCurrentActivity();
    if (activity == null) {
      return;
    }
    activity.runOnUiThread(new Runnable() {
      @Override
      public void run() {
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
      }
    });
  }

}